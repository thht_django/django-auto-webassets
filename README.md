# django-auto-webassets
django-auto-webassets is a [Django](https://www.djangoproject.com/) app that automates the creation of [webassets](https://webassets.readthedocs.io/en/latest/)/[django-assets](https://django-assets.readthedocs.io/en/latest/) bundles. Besides these python libraries, it makes heavy use of [requirejs](https://requirejs.org/).

For detailed information, head to the [documentation](https://django-auto-webassets.readthedocs.io).

